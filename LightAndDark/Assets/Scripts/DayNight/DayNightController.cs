﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public enum DayStages
{
    Dawn,
    Day,
    Dusk,
    Night
}

public enum MoonStages
{
    FullMoon,
    LastQuarter,
    NewMoon,
    FirstQuarter
}

public class DayNightController : MonoBehaviour
{
    public Light2D sunSource;
    [Header("Current World Time")]
    public WorldTime LocalTime;

    public int MinutesInFullDay = 2;
    public int LunarCycleTime = 7;
    private int _secondsInFullDay = 120;
    private int _totalDaysElapsed = 1;
    private int _totalNightsElapsed = 0;

    [Header("Time Of Day")]
    public WorldTime StartTime;
    public WorldTime DawnTime;
    public WorldTime DayTime;
    public WorldTime DuskTime;
    public WorldTime NightTime;

    [Header("Ambient Colors")]
    public Color DuskAmbience;
    public Color DayAmbience;
    public Color DawnAmbience;
    public Color NightAmbience;
    public Color NewMoonNightAmbience;
    public Color QuarterNightAmbience;
    public Color FullMoonNightAmbience;

    [Header("Current Day Stage")]
    public DayStages _currentStage;

    [Header("Current Moon Stage")]
    public MoonStages _currentMoonStage;

    // A multiplier other scripts can use to speed up and slow down the passing of time.
    public float TimeMultiplier = 1f;

    public bool isUnderground;

    private static readonly float _eigthDay = 0.125f;
    private static readonly float _quarterDay = 0.25f;

    private float _newMoonNight = 0f;
    private float _firstQuarterNight = 0.35f;
    private float _fullMoonNight = 0.5f;
    private float _lastQuarterNight = 0.65f;

    private float _dawnTime = 0.20f;
    private float _dayTime = 0.25f;
    private float _duskTime = 0.7f;
    private float _nightTimePre = 0.23f;
    private float _nightTimePost = 0.75f;

    private float _currentTimeOfDay = 0f;
    public float CurrentTimeOfDay { get { return _currentTimeOfDay; } set { _currentTimeOfDay = value; } }
    private float _currentMoonCycleNight = 0f;

    private bool _nightCounterUpdated = false;

    private void Awake()
    {
        _secondsInFullDay = MinutesInFullDay * 60;

        LocalTime = new WorldTime();
        LocalTime.SetTime(StartTime.Hours, StartTime.Minutes);

        _dawnTime = WorldTimeToInt(DawnTime);
        _dayTime = WorldTimeToInt(DayTime);
        _duskTime = WorldTimeToInt(DuskTime);
        _nightTimePre = _dawnTime - 0.03f;
        _nightTimePost = WorldTimeToInt(NightTime);

        _currentTimeOfDay = WorldTimeToInt(LocalTime);
        _currentMoonCycleNight = _totalNightsElapsed / LunarCycleTime;

        if ((_nightTimePost < _duskTime) || (_duskTime < _dayTime) || (_dayTime < _dawnTime))
        {
            Debug.LogError("Day stages have not been correctly setup, disabling script.");
            enabled = false;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        UpdateWorldTime();

        SetDayStage();

        SetMoonStage();

        UpdateAmbientNightColor();

        UpdateAmbientLightColour();

        _currentTimeOfDay += (Time.deltaTime / _secondsInFullDay) * TimeMultiplier;

        if (_currentTimeOfDay >= 1)
        {
            _currentTimeOfDay = 0;
            _totalDaysElapsed++;
            _nightCounterUpdated = false;
        }

        if (_currentTimeOfDay >= 0.7f)
        {
            if (!_nightCounterUpdated)
            {
                if (_totalNightsElapsed >= LunarCycleTime)
                {
                    _totalNightsElapsed = 0;
                }
                else
                {
                    _totalNightsElapsed++;
                }
                _nightCounterUpdated = true;
            }
        }
    }

    void OnGUI()
    {
#if ENABLE_LOGS
        GUI.Label(new Rect(10, 10, 500, 20), "Day " + _totalDaysElapsed.ToString());
        GUI.Label(new Rect(10, 25, 500, 20), "Current Time: " + LocalTime.ToString());
        GUI.Label(new Rect(10, 40, 500, 20), "Day Elapsed: " + (CurrentTimeOfDay * 100).ToString() + "%");
        GUI.Label(new Rect(10, 55, 500, 20), "Night " + _totalNightsElapsed.ToString());
        GUI.Label(new Rect(10, 70, 500, 20), "Lunar Cycle Elapsed: " + (_currentMoonCycleNight * 100).ToString() + "%");
#endif
    }

    private float WorldTimeToInt(WorldTime time)
    {
        return (time.Hours + (time.Minutes / 60f)) / 24f;
    }

    private void SetDayStage()
    {
        if (_currentTimeOfDay <= _nightTimePre || _currentTimeOfDay >= _nightTimePost)
        {
            _currentStage = DayStages.Night;
        }
        else if (_currentTimeOfDay >= _duskTime)
        {
            _currentStage = DayStages.Dusk;
        }
        else if (_currentTimeOfDay >= _dayTime)
        {
            _currentStage = DayStages.Day;
        }
        else if (_currentTimeOfDay >= _dawnTime)
        {
            _currentStage = DayStages.Dawn;
        }
    }

    private void SetMoonStage()
    {
        _currentMoonCycleNight = (float)_totalNightsElapsed / (float)LunarCycleTime;
        if (_currentMoonCycleNight <= _newMoonNight)
        {
            _currentMoonStage = MoonStages.NewMoon;
        }
        else if (_currentMoonCycleNight <= _firstQuarterNight)
        {
            _currentMoonStage = MoonStages.FirstQuarter;
        }
        else if (_currentMoonCycleNight <= _fullMoonNight)
        {
            _currentMoonStage = MoonStages.FullMoon;
        }
        else if (_currentMoonCycleNight <= _lastQuarterNight)
        {
            _currentMoonStage = MoonStages.LastQuarter;
        }
    }

    private void UpdateAmbientNightColor()
    {
        switch (_currentMoonStage)
        {
            case MoonStages.FullMoon:
                NightAmbience = FullMoonNightAmbience;
                break;
            case MoonStages.LastQuarter:
                NightAmbience = QuarterNightAmbience;
                break;
            case MoonStages.NewMoon:
                NightAmbience = NewMoonNightAmbience;
                break ;
            case MoonStages.FirstQuarter:
                NightAmbience = QuarterNightAmbience;
                break;
        }
    }

    private void UpdateAmbientLightColour()
    {
        if (isUnderground)
        {
            RenderSettings.ambientLight = NewMoonNightAmbience;
            return;
        }

        float relativeTime;
        switch (_currentStage)
        {
            case DayStages.Dawn:
                sunSource.color = Color.Lerp(NightAmbience, DawnAmbience, Mathf.Clamp01((_currentTimeOfDay - _dawnTime) * (1 / 0.02f)));
                break;
            case DayStages.Day:
                relativeTime = _currentTimeOfDay - _dayTime;
                sunSource.color = Color.Lerp(DawnAmbience, DayAmbience, relativeTime / (_quarterDay + _eigthDay));
                break;
            case DayStages.Dusk:
                sunSource.color = Color.Lerp(DayAmbience, DuskAmbience, Mathf.Clamp01(((_currentTimeOfDay - _duskTime) * (1 / 0.02f))));
                break;
            case DayStages.Night:
                relativeTime = (_currentTimeOfDay >= _nightTimePost) ? (_currentTimeOfDay - _nightTimePost) / _eigthDay : 1f;
                sunSource.color = Color.Lerp(DuskAmbience, NightAmbience, relativeTime);
                break;
        }
    }

    /// Updates the World-time hour based on the current time of day.  
    private void UpdateWorldTime()
    {
        var fHours = 24 * _currentTimeOfDay;
        var hours = (int)fHours;
        var minutes = (int)(60 * (fHours - Mathf.Floor(fHours)));

        LocalTime.SetTime(hours, minutes);

    }

}

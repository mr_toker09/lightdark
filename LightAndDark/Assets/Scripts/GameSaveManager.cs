﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaveManager : MonoBehaviour
{
    public InventoryObj inventory;
    public BoolValue[] pickUps;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetPickUps()
    {
        for (int i = 0; i < pickUps.Length; i++)
        {
            pickUps[i].RuntimeValue = false;
        }
    }

    public void ClearInventory()
    {
        inventory.items.Clear();
    }
}

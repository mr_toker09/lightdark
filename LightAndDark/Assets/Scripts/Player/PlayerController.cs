﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerState
{
    walk,
    attack,
    interact,
    stagger,
    idle
}

public class PlayerController : MonoBehaviour
{
    public GameObject hero;
    public PlayerState currentState;
    public float moveSpeed;

    public GameObject torch;
    public bool hasTorch;

    public GameObject weapon;
    public bool hasWeapon;

    private float speed;
    private Vector3 change;
    private BoxCollider2D _collider;
    private Rigidbody2D _rigidbody;
    private Animator _anim;
    private VariableJoystick _joystick;
    private Character _character;

    void Awake()
    {
        _collider = GetComponent<BoxCollider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();
        _anim = hero.GetComponent<Animator>();
        _joystick = FindObjectOfType<VariableJoystick>();
        _character = FindObjectOfType<Character>();
    }

    // Start is called before the first frame update
    void Start()
    {
        speed = moveSpeed;
        //Debug.LogError(Application.persistentDataPath.ToString());
    }

    // Update is called once per frame
    void Update()
    {
        change = Vector3.zero;
#if UNITY_EDITOR || UNITY_STANDALONE
        change.x = Input.GetAxisRaw("Horizontal");
        change.y = Input.GetAxisRaw("Vertical");
#elif UNITY_ANDROID
        change.x = _joystick.Horizontal;
        change.y = _joystick.Vertical;
#endif
        HandleTorch();
        HandleWeapon();
    }

    void FixedUpdate()
    {
        if (currentState == PlayerState.walk || currentState == PlayerState.idle)
        {
            UpdateAnimationAndMove();
        }
    }

    void UpdateAnimationAndMove()
    {
        if (change != Vector3.zero)
        {
            MoveCharacter();
            _anim.SetBool("moving", true);
        }
        else
        {
            _anim.SetBool("moving", false);
        }

        SetDirection();
    }

    void MoveCharacter()
    {
        change.Normalize();
        _rigidbody.MovePosition(transform.position + change * speed * Time.deltaTime);
    }

    void SetDirection()
    {
        if (change.x > 0)
        {
            hero.transform.parent.localScale = new Vector3(1f, 1f, 1f);
        }
        else if (change.x < 0)
        {
            hero.transform.parent.localScale = new Vector3(-1f, 1f, 1f);
        }
    }

    void HandleTorch()
    {
        if (hasTorch)
        {
            torch.SetActive(true);
        }
        else
        {
            torch.SetActive(false);
        }
    }

    void HandleWeapon()
    {
        if (hasWeapon)
        {
            weapon.SetActive(true);
        }
        else
        {
            weapon.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarManager : MonoBehaviour
{
    public RectTransform healthBarBGRect;
    public RectTransform healthBarRect;
    public Image healthBar;

    public FloatValue heartContainers;
    public FloatValue playerCurrentHealth;

    private Rect healthBarSize;

    // Start is called before the first frame update
    void Start()
    {
        UpdateHealthBar();
    }

    void InitHealthBar()
    {
        healthBarSize = healthBarBGRect.rect;
        healthBarSize.width = 100f * heartContainers.RuntimeValue;

        healthBarBGRect.sizeDelta = new Vector2(healthBarSize.width, healthBarBGRect.sizeDelta.y);
        healthBarRect.sizeDelta = new Vector2(healthBarSize.width, healthBarRect.sizeDelta.y);
    }

    public void UpdateHealthBar()
    {
        InitHealthBar();
        float _health = playerCurrentHealth.RuntimeValue;
        float _totalHealth = heartContainers.RuntimeValue;
        healthBar.fillAmount = _health / _totalHealth;
    }
}

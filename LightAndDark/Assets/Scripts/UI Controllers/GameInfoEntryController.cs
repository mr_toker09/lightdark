﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameInfoType
{
    InfoUpdate,
    InventoryUpate,
    HealthUpdate,
    LevelUpdate,
    DamageUpdate
}
public class GameInfoEntryController : MonoBehaviour
{
    public Text infoText;
    public Color dayTextColor;
    public Color nightTextColor;
    public float lifetime;

    private DayNightController _dayNight;

    void Awake()
    {
        infoText = GetComponent<Text>();
        _dayNight = FindObjectOfType<DayNightController>();
    }

    private void Start()
    {
        Destroy(this.gameObject, lifetime);
    }
    // Start is called before the first frame update
    public void Populate(GameInfoType infoType, string info )
    {
        Color textColor = new Color();
        switch (_dayNight._currentStage)
        {
            case DayStages.Dawn:
            case DayStages.Day:
                {
                    textColor = dayTextColor;
                }
                break;
            case DayStages.Dusk:
            case DayStages.Night:
                {
                    textColor = nightTextColor;
                }
                break;
        }
        string textToDisplay = "";
        switch (infoType)
        {
            case GameInfoType.InfoUpdate:
                {
                    textToDisplay = info;
                }
                break;
            case GameInfoType.InventoryUpate:
                {
                    textToDisplay = info + " added";
                }
                break;
        }

        infoText.color = textColor;
        infoText.text = textToDisplay;
    }
}

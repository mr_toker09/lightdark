﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceNameController : MonoBehaviour
{
    public Text placeNameText;
    private Animator _anim;

    void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    public void SetPlaceName(string placeName)
    {
        placeNameText.text = placeName;
        _anim.SetTrigger("ACTIVE");
    }
}

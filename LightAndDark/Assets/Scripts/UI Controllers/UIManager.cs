﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject gameHUD;
    public GameObject characterPanel;
    public GameObject pauseScreen;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Inventory"))
        {
            if (characterPanel.activeInHierarchy)
            {
                characterPanel.SetActive(false);
            }
            else
            {
                characterPanel.SetActive(true);
            }
        }
    }
}

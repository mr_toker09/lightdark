﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfoDisplayController : MonoBehaviour
{
    public Transform parentContainer;
    public GameInfoEntryController gameInfoEntryPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateEntry(GameInfoType infoType, string info)
    {
        GameInfoEntryController entry = (GameInfoEntryController)Instantiate(gameInfoEntryPrefab, transform.position, transform.rotation);
        entry.gameObject.transform.SetParent(parentContainer);
        entry.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
        entry.Populate(infoType, info);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomMove : MonoBehaviour
{
    private CameraMovement _camera;

    public Vector2 cameraChange;
    public Vector3 playerChange;

    public bool needText;
    public string placeName;

    private PlaceNameController _placeName;

    private void Awake()
    {
        _camera = Camera.main.GetComponent<CameraMovement>();
        _placeName = FindObjectOfType<PlaceNameController>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _camera.minPosition += cameraChange;
            _camera.maxPosition += cameraChange;
            other.transform.position += playerChange;
            if (needText)
            {
                _placeName.SetPlaceName(placeName);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPicker : Interactable
{
    [Header("Contents")]
    public Item contents;
    public bool isOpen;
    public BoolValue storedOpen;

    [Header("Feedback")]
    public GameObject collectedFX;
    public GameInfoDisplayController gameInfo;

    [Header("Display")]
    public bool isChest;
    public SpriteRenderer chestSprite;
    public Sprite closedChest;
    public Sprite openChest;

    private Character character;
    public ItemContainer itemContainer;

    void Awake()
    {
        character = FindObjectOfType<Character>();
    }

    // Use this for initialization
    void Start()
    {
        isOpen = storedOpen.RuntimeValue;
        HandleChestDisplay();
    }

    private void HandleChestDisplay()
    {
        if (!isChest)
            return;

        if (isOpen)
        {
            chestSprite.sprite = openChest;
        }
        else
        {
            chestSprite.sprite = closedChest;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && playerInRange)
        {
            if (!isOpen)
            {
                OpenChest();
            }
        }
    }

    bool CheckInventorySpace()
    {
        if (!itemContainer.CanAddItem(contents, 1))
        {
            Debug.LogError("Your inventory is full.");
            return false;
        }
        return true;
    }

    public void OpenChest()
    {
        if (!CheckInventorySpace())
        {
            gameInfo.CreateEntry(GameInfoType.InfoUpdate, "Inventory full");
            return;
        }

        Instantiate(collectedFX, this.transform.position, Quaternion.identity);
        character.Inventory.AddItem(contents);
        gameInfo.CreateEntry(GameInfoType.InventoryUpate, contents.ItemName);
        contextOff.Raise();
        isOpen = true;
        storedOpen.RuntimeValue = isOpen;
        HandleChestDisplay();
        character.SavePlayerInventory();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger && !isOpen)
        {
            contextOn.Raise();
            playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && !other.isTrigger && !isOpen)
        {
            contextOff.Raise();
            playerInRange = false;
        }
    }
}

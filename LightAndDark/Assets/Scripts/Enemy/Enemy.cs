﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health;
    public string enemyName;
    public float baseAttack;
    public float moveSpeed;

    public Transform target;
    public float chaseRadius;
    public float attackRadius;
    public Transform homePosition;

    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        CheckDistance();
    }

    void CheckDistance()
    {
        Debug.Log("Player distance: " + Vector2.Distance(target.position, this.transform.position));
        if ((Vector2.Distance(target.position, this.transform.position) <= chaseRadius) &&
            (Vector2.Distance(target.position, this.transform.position) > attackRadius))
        {
            this.transform.position = Vector2.MoveTowards(this.transform.position, target.position, moveSpeed * Time.deltaTime);
        }
        else
        {
            if(homePosition != null)
                this.transform.position = Vector2.MoveTowards(this.transform.position, homePosition.position, moveSpeed * Time.deltaTime);
        }
    }
}

﻿using System.Collections.Generic;
using UnityEngine;

public class ItemSaveManager : MonoBehaviour
{
	[SerializeField] ItemDatabase itemDatabase;

	private const string InventoryFileName = "Inventory";
	private const string EquipmentFileName = "Equipment";

	public void LoadInventory(Character character)
	{
		for (int i = 0; i < character.inventoryObj.items.Count; i++)
		{
			ItemSlot itemSlot = character.Inventory.ItemSlots[i];
			ItemSlotSaveInfo saveInfo = character.inventoryObj.items[i];

			itemSlot.Item = itemDatabase.GetItemCopy(saveInfo.ItemID);
			itemSlot.Amount = saveInfo.Amount;
		}
	}

	public void LoadEquipment(Character character)
	{
        List<ItemSlotSaveInfo> _equipmentItems = new List<ItemSlotSaveInfo>(character.equipmentObj.items);

        foreach (ItemSlotSaveInfo saveInfo in _equipmentItems)
		{
			if (saveInfo == null) {
				continue;
			}

			Item item = itemDatabase.GetItemCopy(saveInfo.ItemID);
			character.Inventory.AddItem(item);
			character.Equip((EquippableItem)item);            
        }
	}

	public void SaveInventory(Character character)
	{
		SaveItems(character.Inventory.ItemSlots, InventoryFileName, character.inventoryObj);
	}

	public void SaveEquipment(Character character)
	{
		SaveItems(character.EquipmentPanel.EquipmentSlots, EquipmentFileName, character.equipmentObj);
	}

	private void SaveItems(IList<ItemSlot> itemSlots, string fileName, InventoryObj inventoryObj)
	{
        inventoryObj.items.Clear();
        for (int i = 0; i < itemSlots.Count; i++)
        {
            ItemSlot itemSlot = itemSlots[i];

            if (itemSlot.Item != null)
            {
                inventoryObj.AddItem(itemSlot.Item.ID, itemSlot.Amount);
            }
        }
    }
}

﻿using UnityEngine;

[CreateAssetMenu(menuName = "Item Effects/Heal")]
public class HealItemEffect : UsableItemEffect
{
	public float HealAmount;

	public override void ExecuteEffect(UsableItem usableItem, Character character)
	{
        character.UpdateHealth(HealAmount, true);
	}

	public override string GetDescription()
	{
		return "Heals for " + HealAmount + " health.";
	}
}

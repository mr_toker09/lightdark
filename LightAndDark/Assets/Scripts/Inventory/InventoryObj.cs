﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemSlotSaveInfo
{
    public string ItemID;
    public int Amount;

    public ItemSlotSaveInfo(string id, int amount)
    {
        ItemID = id;
        Amount = amount;
    }
}

[CreateAssetMenu(fileName = "New Inventory", menuName = "SaveData/InventoryObj")]
[System.Serializable]
public class InventoryObj : ScriptableObject
{
    public List<ItemSlotSaveInfo> items = new List<ItemSlotSaveInfo>();

    public void AddItem(string id, int amount)
    {
        ItemSlotSaveInfo saveItem = new ItemSlotSaveInfo(id, amount);
        items.Add(saveItem);
        Debug.LogError("Adding item");
    }
}

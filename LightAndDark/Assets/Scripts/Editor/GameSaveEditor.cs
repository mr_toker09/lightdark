﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameSaveManager))]
public class GameSaveEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GameSaveManager _gameSave = (GameSaveManager)target;

        if (GUILayout.Button("Reset Pickups"))
        {
            _gameSave.ResetPickUps();
        }

        if (GUILayout.Button("Clear Inventory"))
        {
            _gameSave.ClearInventory();
        }
    }
}
